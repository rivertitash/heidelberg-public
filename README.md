# heidelberg-public

This is the public facing repo for the salesforce conversational analytics project, codenamed Heidelberg. This project provides a chat based interface for asking questions to salesforce
and provides an electron frontend. The current version has a docker based backend, and the entire data never leaves the user's desktop or laptop. 

## Version information

The current version is 0.01 Build 100

## Getting Started Information

If you want to download and evaluate the project as a end user, check out the Get Started information [here]. (https://bitbucket.org/rivertitash/heidelberg-public/src/7b383aa02c3798218e37bf8ad9cc03605dab2fc4/get-started.md?at=master)

## Motivation and Project Background

Heidelberg uses Knowledge Graphs to solve the problem of conversational analytics for Salesforce. Below we discuss the main reasons for why such a tool might be relevent for today. 

#### Behavioural shift in people due to technology changes
The future of human-computer interaction is going to be governed less by GUI and more by Character User Interfaces (CUI) and Voice User Interfaces (VUI).
There are multiple forces at work and in convergence which will cause this shift to happen. Behavioural forces like the diminishing ability of humans to make complex decisions, outsourcing them to machines and engineering advances, such as those in Neural Networks, Augmented reality etc. will mean a underlying shift in the way people go about using information for decision making. A good example of this is how, we no longer rely on human map reading skills for finding directions, but are increasingly using Google Maps.

#### Need to harness Tribal knowledge / community knowledge to solve problems of cold-start and lack of expertise
AI powered systems can detect and build a global repository of domain specific KPIs or metrics to be tracked, based on other businesses tracking them, etc. There is already a lot of content (articles, best practices, blog posts) for every niche of analytics, created by the community. A global domain-specific repository can be used to harness this knowledge in an actionable way, and plug it back to first timers or people who are not aware of specific KPIs relevant to them.
This is the analytics equivalent of “People who bought this, also found this useful”.

#### Opportunity for personalisation down to the segment of one
Because AI powered systems are capable of adapting to users needs and usage patterns, it is possible to design for personalisation down to each user on the system. This personalisation can range from mapping the users personal lingo to that of the company or the domain or understanding the role that user performs in the organisation and what his important metrics are, listening to those specific metrics and providing them.
Personalisation also refers to understanding the context in which a user is asking a specific question or demanding some metrics, be keeping track of previous usage or conversations over a period of time - for example, is the user asking for revenue numbers before a VC presentation for all his products or before a competitor analysis meeting for a specific vertical.




