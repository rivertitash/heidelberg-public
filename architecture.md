## Why Knowledge Graph approach

The core problem we are trying to solve in a scalable, automated fashion is the ability for a system to understand entities, attributes and relationships.
Given a human question about an underlying data set, the system has to

1.  Understand the users question and his context.    
2.  Identify the right dataset, amongst multiple datasets.
3.  Make sense of the underlying entities and map them to something meaningful in a dataset.
4.  Identify the operations to be performed to complete the users questions, on those entities.
    
## Why not just a Neural Network ?

If we try to use Deep learning, to train this system, we will have a problem of cold-start, because to begin with we may not have a single large homogenous dataset to train a meaningful NN model.
Second, the core problem of identifying entities in different contexts, still remains and cannot be solved by Deep Learning and with Machine Learning, will become very probabilistic at best. We could try to use a triage of different algorithms to identify things, but we would still need to store entities, attributes and relationships in a central database.

Also, because the objective is to build something for a SaaS based world, we have to account for something that does not depend on a large dataset but grows with each usage by discovering new things about the domain.
Finally, it is important to not just understand the core entities but also to understand and recall context of both the underlying data and the user himself. When a user asks a question, the system should be able to immediately understand and map the terminology used by the user to the entities, operations, etc. and give an answer

  

## The graph is the db
At the core of this approach is an analytics focussed knowledge graph, with the following key features -
-   The core ontology and graph will be open source and available publicly (similar to or extensions of Unigraph, Grakn, etc.)    
-   Every user on the system will have their own closed sections of the graph with mappings to the core - enabling one way translation by a system. This will allow personalisation without losing the power of the graph.


## Basic structure of query parsing

  
Every user question in conversational analytics can be parsed into the following structure. 

    Class : operation : property : filter (time)

> Operation is performed on Class for a property and filter

##### Examples

    **What is the average deal size**
    {class: deal -> opportunity}
    {operation: average}
    {property: size -> amount}

    **What is the total opportunity**
    {class: opportunity}
    {operation : total }
    {property : ND / default}
    {filter: ND / default }  

    **How much did sales grow this quarter compared to last quarter**
    {class: sales}
    {operation: grow -> growth -> total value for current period - total value for previous period / total value for previous ] X 100 }
    {property: amount}
    {filter: period (default is quarter or specified by user}

    **How are targets looking for this quarter compared to last quarter**
    {class: sales}
    {operation: target -> (Sales for the current period/Sales target)x100 }
    {property: amount}
    {filter: period / default quarter }


**Default property of operation -** 
For every operation there will be one or more default properties that are taken or assumed if none is specified by the user.
For example, for the operation total - the default will always be numeric values (amount, days, etc. )

**Default property of class -** For every class, there will always be one and only one default property, denoting the most important or relevant characteristic of the class.
For example, for class opportunity the default class will always be amount.

**Order of precedence -** Default value of class will take precedence over default value of operation. User provided values will take precedence over everything.

KG representation - Default values will be represented in the KG like so (see bold) :

    {"name":"total","where":true,"expression":"sum(n.$targetColumn)","para│
    │ms":["targetColumn","targetTable"],"required":"numeric", “default-value” : “**amount**”}

