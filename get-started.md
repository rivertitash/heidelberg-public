
# heidelberg-public

This is the public facing repo for the salesforce conversational analytics project, codenamed Heidelberg. This project provides a chat based interface for asking questions to salesforce
and provides an electron frontend. The current version has a docker based backend, and the entire data never leaves the user's desktop or laptop. 

## Version information

The current version is 0.01 Build 100

## Dependencies

*   MacOS X 2010 Upwards with Moutain Lion or above
*   Windows 10 (Not currently tested)
* Ubuntu Linux 16 and above
* Docker and Docker compose. Download here - 
	* [Windows](https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe)
	* [Mac](https://download.docker.com/mac/stable/Docker.dmg) 
* Salesforce account on an existing salesforce org with [Bulk API Access Enabled](https://developer.salesforce.com/forums/?id=906F00000008lUuIAI)

## Installation Steps

#### Downloading the relevent files
1. Download and install Docker from the link under dependencies. 
2. Download the Electron Frontend from here -> https://drive.google.com/drive/folders/15X-GGUPWFOi-ua9aRwvrq-nOOilNeBy7?usp=sharing
3. Download the file docker-compose.yml -> https://drive.google.com/drive/u/0/folders/15X-GGUPWFOi-ua9aRwvrq-nOOilNeBy7



#### Running the backend 
4. If you are on Mac - Click on ***Spotlight***  type  ***Terminal*** Once the terminal window opens, navigate to the folder you have downloaded the dockerfile and run ***docker compose up***
5. If you are on Windows - Press **Win** + R keys on your keyboard. Then, type **cmd** or **cmd**.exe and press Enter or click/tap OK. navigate to the folder you have downloaded the dockerfile and run ***docker compose up*** 
6. If Docker is installed and running, then the Heidelberg backend will start and the console log will indicate that both Neo4J and Heidelberg backend is now available. Your console logs should as below

![Imgur](https://i.imgur.com/aUfSSVX.png)
![Imgur](https://i.imgur.com/wnO5orN.png)




#### Running the frontend chat bot

![Imgur](https://i.imgur.com/WRsAj3y.png)

8. Now double-click and launch the electron frontend. You should see as shown in screehshot above.
9. The current version of the bot can run in two modes - Demo and Live
10. In the ***Demo Mode*** pre-loaded sample data is used to demonstrate how the bot works with Salesforce and the kind of questions it can currently answer. 
11. In the ***Live Mode*** the bot will import your Salesforce data, after OAuth and then run the same questions for your own data. For details on Setting up live mode and OAuth, refer to salesforce-auth.md  
12. Enjoy chatting with the bot or file an issue here!

***Note*** - Heidelberg is still in very early stages of development and has limited functionality. Its not ready for wide-scale public use. We welcome you to look up the project architecture and extend this for your own purposes. 



	
	
