Heidelberg currently works with Salesforce data. There are three type of account in salesforce: 

- Trial for organisation - API service is not present
- Enterprise for organisation (Paid) - API service is present
- Developer Edition (Mini Enterprise edition) - API service is present


** Importing data from Salesforce **

Heidelberg uses a Neo4J based graph database to answer queries about your salesforce data. To read more on how this works, refer to the architecture and other docs (TBD).

To make it work for your Salesforce installation, follow the steps below:

1. Ensure you have API access enabled on your account. To find out more about API Access [visit this url.](https://ebstalimited.zendesk.com/hc/en-us/articles/229295368-How-do-I-enable-API-access-in-Salesforce-)
2. Once API access is enabled, on the Chatbot, click on ***Salesforce*** - You will see a dialog asking you to authorise Heidelberg to Salesforce. 
3. Once your login details are accepted, salesforce allows Heidelberg to import all the data into the graph database. 
4. The bulk import process starts automatically, as soon as the oauth is succesful. Depending on how big your DB on Salesforce is, this might take between 3 to 7 minutes. 
5. Heidelberg will automatically detect the schema of your SF installation. At the time of writing this doc, Heidelberg only works with standard SF Schemas. 
6. Once the import is finished, you will be prompted by the bot and it will let you ask questions about your data. 

***Note*** Currently Heidelberg only supports a few limited types of questions as its Knowledge Graph is still work in progress. Future versions will include a self-learning graph system. 


